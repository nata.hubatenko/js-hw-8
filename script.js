// знаходимо всі параграфи
const allPar = document.querySelectorAll('p');
console.log(allPar);

//встановлюємо колір фону
for (const element of allPar) {
	element.style.backgroundColor = '#ff0000';
}

// елемент із id
const elemId = document.getElementById ('optionsList');
console.log(elemId);


//батьківський елемент
const parentId = elemId.parentNode;
console.log(parentId);

//дочірні ноди
const childId = elemId.childNodes;
for (let i=0; i<childId.length; i++)
{
	console.log(childId[i].nodeName);
	console.log(childId[i].nodeType);
}


//додаємо контент
const testParagraph = document.getElementsByClassName("testParagraph");
testParagraph.createTextNode = "This is a paragraph";


//перейменовуємо клас
const mainHeader = document.querySelectorAll(".main-header");
console.log(mainHeader);

for (const element of mainHeader) {
	element.classList.add('nav-item');
}


const optionsList = document.getElementsByClassName('options-list-title');
console.log(optionsList);



